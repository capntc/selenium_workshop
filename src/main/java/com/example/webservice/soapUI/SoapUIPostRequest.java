package com.example.webservice.soapUI;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import com.example.core.exceptions.BFInputDataException;
import com.example.selenium.core.exceptions.BFComponentStateException;

/**
 * This class represents POST request over http
 * 
 * @author
 *
 */
public class SoapUIPostRequest {

	private HttpPost postRequest;
	private HttpClient httpClient;

	protected SoapUIPostRequest(String url) {
		httpClient = HttpClientBuilder.create().build();
		postRequest = new HttpPost(url);
	}

	/**
	 * Attaches xml to this request
	 * 
	 * @param xml
	 *            to attach
	 * @return current {@link SoapUIPostRequest}
	 */
	public SoapUIPostRequest attachXml(String xml) {
		StringEntity input;
		try {
			input = new StringEntity(xml);
		} catch (UnsupportedEncodingException e) {
			throw new BFInputDataException("Provided string encoding was not recognized.");
		}
		input.setContentType("text/xml");
		postRequest.setEntity(input);
		return this;
	}

	/**
	 * Sends POST request
	 * 
	 * @return response - instance of {@link SoapUIPostResponse}
	 */
	public SoapUIPostResponse send() {
		HttpResponse response;
		try {
			response = httpClient.execute(postRequest);
		} catch (ClientProtocolException e) {
			throw new BFComponentStateException("POST request", "send", "signaling an error in HTTP protocol.");
		} catch (IOException e) {
			throw new BFComponentStateException("POST request", "send", "signaling an I/O exception.");
		}
		return new SoapUIPostResponse(response);
	}

}
