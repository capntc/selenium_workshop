package com.example.webservice.soapUI;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import com.example.selenium.core.exceptions.BFComponentStateException;
import com.example.selenium.core.utils.StringUtils;

import org.apache.http.HttpResponse;

/**
 * This class represents response returned after Htpp POST request
 * 
 * @author
 *
 */
public class SoapUIPostResponse {

	private String content;
	private final HttpResponse response;
	private final String fieldRegexPattern = "(<[^/].*?%s>)(.*?)(</.*?%s>)";

	protected SoapUIPostResponse(HttpResponse response) {
		this.response = response;
	}

	/**
	 * Retrieves text from between specified tags
	 * 
	 * @param tagName
	 * @return text found between specified tags
	 */
	public List<String> getContentByTagName(String tagName) {
		String regex = String.format(fieldRegexPattern, tagName, tagName);
		return StringUtils.findSubstrings(getContent(), regex, 2);
	}

	private String getContent() {
		if (content == null) {
			extractContent();
		}
		return content;
	}

	private void extractContent() {
		InputStream is;
		try {
			is = response.getEntity().getContent();
		} catch (UnsupportedOperationException e) {
			throw new BFComponentStateException(
					"POST response",
					"retrieve content from",
					"unable to execute operation.");
		} catch (IOException e) {
			throw new BFComponentStateException(
					"POST response",
					"retrieve content from",
					"signaling an I/O exception.");
		}
		content = getStringFromInputStream(is);
	}

	private String getStringFromInputStream(InputStream is) {
		BufferedReader reader = null;
		StringBuilder builder = new StringBuilder();

		try {
			String line;
			reader = new BufferedReader(new InputStreamReader(is));
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return builder.toString();
	}
}
