package com.example.webservice.soapUI;

/**
 * This class 'emulates' usage of SoapUI. It enables sending requests over http
 * 
 * @author
 *
 */
public class SoapUIUtils {

	private SoapUIUtils() {
	}

	/**
	 * Creates POST request
	 * 
	 * @param url
	 * @return instance of {@link SoapUIPostRequest}
	 */
	public static SoapUIPostRequest post(String url) {
		return new SoapUIPostRequest(url);
	}
}
