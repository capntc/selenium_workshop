package com.example.webservice.pageobject;

public class UrlServiceObjectModel implements IUrlServiceObjectModel {
	
	private String path;
	private IUrlServiceObjectModel parent;
	
	public UrlServiceObjectModel(String path) {
		this(null, path);
	}
	
	public UrlServiceObjectModel(IUrlServiceObjectModel parent, String path) {
		this.path = path;
		this.parent = parent;
	}
	
	@Override
	public IUrlServiceObjectModel getParent() {
		return this.parent;
	}
	
	@Override
	public void setParent(IUrlServiceObjectModel parent) {
		this.parent = parent;
	}
	
	public String getPath() {
		StringBuilder stringBuilder = new StringBuilder();
		if (parent != null) {
			stringBuilder.append(parent.getPath());
		}
		
		stringBuilder.append("/" + path);
		
		return stringBuilder.toString();
	}
	
}
