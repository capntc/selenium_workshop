package com.example.webservice.pageobject;

interface IUrlServiceObjectModel {
	
	IUrlServiceObjectModel getParent();
	
	void setParent(IUrlServiceObjectModel parent);
	
	String getPath();
	
}
