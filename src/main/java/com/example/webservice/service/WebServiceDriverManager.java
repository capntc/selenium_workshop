package com.example.webservice.service;

import static com.example.core.base.environments.ParametersManager.parameters;
import static io.restassured.RestAssured.given;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import com.example.core.logger.BFLogger;
import com.example.webservice.service.sesion.sso.SSOSession;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.specification.RequestSpecification;

public class WebServiceDriverManager {

	private static ThreadLocal<RequestSpecification> drivers = new ThreadLocal<RequestSpecification>();

	public static RequestSpecification getRequestSpecyfication() {
		RequestSpecification requestSpecification = drivers.get();
		if (requestSpecification == null) {
			requestSpecification = createDriver();
			drivers.set(requestSpecification);
			requestSpecification.spec(getAuthenticationData());
		}
		return requestSpecification;
	}

	/**
	 * Method sets desired 'driver' depends on chosen parameters
	 */
	private static RequestSpecification createDriver() {
		BFLogger.logDebug("Creating new webservice driver");
		return given();
	}

	private static RequestSpecification getAuthenticationData() {
		switch(parameters().getAuthenticationType()){
		default:
			return getSSOAuthenticationData();
		}
	}
	
	private static RequestSpecification getSSOAuthenticationData() {
		RequestSpecification spec = null;
		try {
			spec = new RequestSpecBuilder().setRelaxedHTTPSValidation()
					.addFilter(new ResponseLoggingFilter()).setRelaxedHTTPSValidation()
					.addCookies(SSOSession.getRestAssuredCookies()).build();
			return spec;
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | IOException e) {
			BFLogger.logError("Session configuration fails" + e.getMessage());
			e.printStackTrace();
		}
		return spec;	
	}
}
