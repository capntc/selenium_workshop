package com.example.database.databank;

import static com.example.database.utils.ConfigurationReader.read;

import java.util.HashMap;
import java.util.Map;

/**
 * Class with data needed into database package
 * 
 * @author PWOJTKOW
 */
public class DataBank {
	final public static String DB_CONFIG_FILE_PATH = read("settings.properties").getProperty("db.config");
	final public static String ORACLE_DRIVER_NAME = "oracle.jdbc.OracleDriver";
	final public static String CASSANDRA_DRIVER_NAME = "org.bigsql.cassandra2.jdbc.CassandraDriver";
	
	private static Map<String, String> dbDrivers;
	
	static {
		dbDrivers = new HashMap<String, String>();
		dbDrivers.put("oracle", ORACLE_DRIVER_NAME);
		dbDrivers.put("cassandra", CASSANDRA_DRIVER_NAME);
	}

	/**
	 * @param name
	 *            - database name, which can be used in database configuration file
	 * @param driverName
	 *            - database driver class name assigned to database name
	 */
	public static void addDriverType(String name, String driverName) {
		dbDrivers.put(name, driverName);
	}

	public static Map<String, String> getDbDrivers() {
		return dbDrivers;
	}
	
}
