package com.example.database.demo;

import com.example.database.base.BaseDatabase;
import com.example.database.demo.dbObjects.OracleEmployeesTable;

/**
 * Example of BaseDatabase class implementation.
 * 
 * @author PWOJTKOW
 *
 */
public class OracleDatabaseDemo extends BaseDatabase {

	// Prefix is defined in configuration file. If dbPrefix value is not set, prefix with "default" value from configuration file will be used 
	private final static String dbPrefix = "db1";
	private OracleEmployeesTable emplTable = new OracleEmployeesTable(dbPrefix);
	
	public OracleDatabaseDemo () {
		super();
	}
	
	public static String getDbprefix() {
		return dbPrefix;
	}

	public OracleEmployeesTable getEmplTable() {
		return emplTable;
	}
	
	
}
