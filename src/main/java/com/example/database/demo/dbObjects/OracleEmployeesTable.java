package com.example.database.demo.dbObjects;

import com.example.database.demo.queries.AllEmployeesOracle;

public class OracleEmployeesTable {

	private AllEmployeesOracle allEmployeesQuery;

	public OracleEmployeesTable(String dbPrefix) {
		allEmployeesQuery = new AllEmployeesOracle(dbPrefix);
	}
	
	public AllEmployeesOracle getAllEmployeesQuery() {
		return allEmployeesQuery;
	}
	
}
