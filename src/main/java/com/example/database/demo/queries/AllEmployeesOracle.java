package com.example.database.demo.queries;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.example.database.utils.Query;

/**
 * Example of Query utility class usage. Based on PL/SQL example table "Employees".
 * @author PWOJTKOW
 *
 */
public class AllEmployeesOracle extends Query {
	
	private final String LAST_NAME_COLUMN = "LAST_NAME";
	private final String FIRST_NAME_COLUMN = "FIRST_NAME";
	private final String EMPLOYEE_ID_COLUMN = "EMPLOYEE_ID";
	
	private List<Integer> employeesIds = new ArrayList<Integer>();
	private List<String> employeesFirstNames = new ArrayList<String>();
	private List<String> employeesLastNames = new ArrayList<String>();
	
	public AllEmployeesOracle(String dbPrefix) {
		super(dbPrefix, "SELECT * FROM EMPLOYEES");
	}
	
	@Override
	protected void process() throws SQLException {
		while (resultSet.next()) {
			employeesIds.add(resultSet.getInt(EMPLOYEE_ID_COLUMN));
			employeesFirstNames.add(resultSet.getString(FIRST_NAME_COLUMN));
			employeesLastNames.add(resultSet.getString(LAST_NAME_COLUMN));
		}
	}

	public List<Integer> getEmployeesIds() {
		return employeesIds;
	}

	public List<String> getEmployeesFirstNames() {
		return employeesFirstNames;
	}

	public List<String> getEmployeesLastNames() {
		return employeesLastNames;
	}
	
}
