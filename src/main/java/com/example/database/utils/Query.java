package com.example.database.utils;

import static com.example.database.DbDriverManager.getDriverName;
import static com.example.database.utils.ParameterManager.getDbparameters;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.example.core.logger.BFLogger;

/**
 * Template design pattern for query execution. Includes with database connection opening and closing.
 * @author PWOJTKOW
 *
 */
public abstract class Query {
	
	protected String sql;
	protected Connection connection;
	protected Statement statement;
	protected ResultSet resultSet;
	protected String driverName;
	protected String dbPrefix; 
	
	public Query(String dbPrefix, String sql) {
		this.sql = sql;
		this.dbPrefix = dbPrefix;
		this.driverName = getDriverName(dbPrefix);
	}
	
	public void execute() {
		try {
			connectToDatabase();
			executeSql();
			process();
		} catch (ClassNotFoundException | SQLException e) {
			BFLogger.logDebug("Unable to execute query: " + this.sql + ". " + e.getMessage());
		} finally {
			try {
				close();
			} catch (SQLException e) {
				BFLogger.logDebug("Unable to close sql connections. " + e.getSQLState() 
				+ ", with error code: " + e.getErrorCode() + ", " + e.getMessage());
			}
		}
	}

	private void connectToDatabase() throws ClassNotFoundException, SQLException {
		ParameterHolder parameters = getDbparameters().get(dbPrefix);
		String hostname = parameters.getHostname();
		String username = parameters.getUsername();
		String password = parameters.getPassword();
		
		Class.forName(driverName);
		connection = DriverManager.getConnection(hostname, username, password);
	}
	
	private void executeSql() throws SQLException {
		statement = connection.createStatement();
		resultSet = statement.executeQuery(sql);
	}
	
	private void close() throws SQLException {
		resultSet.close();
		statement.close();
		connection.close();
	}

	abstract protected void process() throws SQLException;
	
}
