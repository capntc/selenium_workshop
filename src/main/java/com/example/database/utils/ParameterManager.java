package com.example.database.utils;

import static com.example.database.databank.DataBank.DB_CONFIG_FILE_PATH;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * Utility class to manage properties from configuration file. Structure of file
 * to read: <database prefix>.<property name>=<property value>
 * 
 * @author PWOJTKOW
 *
 */
public class ParameterManager {

	// prefix = database name inf config file, value = property value from file
	// (example: mydb.username = john --> prefix = mydb, value= john)
	private static Properties config;
	private static Map<String, ParameterHolder> dbparameters;
	private final static String CONFIGURATION_FILE_PATH = DB_CONFIG_FILE_PATH;

	static {
		config = ConfigurationReader.read(CONFIGURATION_FILE_PATH);
		mapInit();
	}

	private static void mapInit() {
		dbparameters = new HashMap<String, ParameterHolder>();
		Set<String> propertiesSet = config.stringPropertyNames();

		for (String prop : propertiesSet) {
			if (prop.contains(".")) {
				int indexOfDbPrefix = prop.lastIndexOf(".");

				String dbPrefix = prop.substring(0, indexOfDbPrefix);
				String property = prop.substring(indexOfDbPrefix + 1);
				String value = config.getProperty(prop);

				ParameterHolder parameterHolder = dbparameters.get(dbPrefix);
				if (parameterHolder == null) {
					parameterHolder = new ParameterHolder();
					dbparameters.put(dbPrefix, parameterHolder);
					addParam(property, value, parameterHolder);
				} else {
					addParam(property, value, parameterHolder);
				}
			}
		}
	}

	private static void addParam(String property, String value, ParameterHolder par) {
		switch (property) {
			case "username" :
				par.setUsername(value);
				break;
			case "password" :
				par.setPassword(value);
				break;
			case "hostname" :
				par.setHostname(value);
				break;
			case "database" :
				par.setDatabase(value);
				break;
			default :
				break;
		}
	}

	public static Properties getConfig() {
		return config;
	}

	public static Map<String, ParameterHolder> getDbparameters() {
		return dbparameters;
	}

}
