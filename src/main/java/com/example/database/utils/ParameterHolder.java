package com.example.database.utils;

public class ParameterHolder {

	private String username;
	private String password;
	private String hostname;
	private String database;
	
	public ParameterHolder() {
	}
	
	public ParameterHolder(String username, String password, String hostname, String database) {
		this.username = username;
		this.password = password;
		this.hostname = hostname;
		this.database = database;
	}
	
	public String getUsername() {
		return username;
	}
	
	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	public String getPassword() {
		return password;
	}
	
	public String getHostname() {
		return hostname;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	
	
	
}
