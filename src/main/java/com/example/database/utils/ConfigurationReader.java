package com.example.database.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.example.core.logger.BFLogger;

/**
 * Utilities class needed to read configuration from configuration file.
 * @author PWOJTKOW
 *
 */
public class ConfigurationReader {
	
	public static Properties read(String configFilePath) {
		
		Properties prop = new Properties();
		InputStream input = null;
		
		try {
			input = new FileInputStream(configFilePath);
			prop.load(input);
			
			return prop;
			
		} catch (IOException ex) {
			BFLogger.logDebug("Unable to read configuration file from path: " + configFilePath + ". "
					+ ex.getMessage());
			return null;
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					BFLogger.logDebug("Unable to close input stream. " + e.getMessage());
				}
			}
		}
	}
	
}
