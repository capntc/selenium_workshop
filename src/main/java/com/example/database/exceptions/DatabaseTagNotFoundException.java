package com.example.database.exceptions;

import java.util.NoSuchElementException;

import com.example.core.logger.BFLogger;

public class DatabaseTagNotFoundException extends NoSuchElementException {

	private static final long serialVersionUID = 1L;
	
	public DatabaseTagNotFoundException(String message) {
		super(message);
		BFLogger.logDebug(message);
	}

}
