package com.example.database;

import static com.example.database.databank.DataBank.getDbDrivers;
import static com.example.database.utils.ParameterManager.getDbparameters;

import com.example.core.logger.BFLogger;

public class DbDriverManager {

	private static ThreadLocal<String> dbdrivers = new ThreadLocal<String>();

	public static String getDriverName(String dbPrefix) {
		String driverName = dbdrivers.get();
		if (driverName == null) {
			driverName = createDriverName(dbPrefix);
			dbdrivers.set(driverName);
		}
		return driverName;
	}

	private static String createDriverName(String dbPrefix) {
		checkIsDefault(dbPrefix);
		String driverName = null;
		String databaseName = null;
		
		try {
			databaseName = getDbparameters().get(dbPrefix).getDatabase();
			driverName = getDbDrivers().get(databaseName);
			BFLogger.logDebug("Creating new: " + databaseName + " Database Driver.");
			return driverName;
		} catch (NullPointerException e) {
			BFLogger.logError("Cannot create new driver. Prefix: " + dbPrefix + 
					", Database Name: " + databaseName + 
					", Driver Name: " + driverName);
		}
		
		return driverName;
	}

	private static void checkIsDefault(String dbPrefix) {
		if (dbPrefix.equals("default")) {
			BFLogger.logDebug("Default value driver is creating");
		}
	}
}
