package com.example.database.base;

/**
 * Base class to create new database representation object. Database can contain
 * tables from dbObjects package.
 * 
 * @author PWOJTKOW
 *
 */
public abstract class BaseDatabase {

	protected final static String dbPrefix = "default";
	
}
