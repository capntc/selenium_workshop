package com.example.core.utils;

public interface IColorValidator<T> {

	boolean isValid(T elementUndertest);

}
