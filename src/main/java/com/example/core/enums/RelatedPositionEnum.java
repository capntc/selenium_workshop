package com.example.core.enums;

public enum RelatedPositionEnum {
	ABOVE,
	BELOW,
	BEFORE,
	AFTER,
	INLINE;
}
