package com.example.core.enums;

public interface IResolutionList {

	public int getWidth();

	public int getHeight();

	public String toString();
}
