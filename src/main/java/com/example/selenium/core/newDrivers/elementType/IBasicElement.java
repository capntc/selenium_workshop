package com.example.selenium.core.newDrivers.elementType;

import org.openqa.selenium.WebElement;

public interface IBasicElement {

	String getElementTypeName();

	WebElement load();

}
