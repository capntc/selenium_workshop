package com.example.selenium.core.newDrivers.elementType;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

public class InputTextElement extends BasicElement implements IBasicElement {

	public InputTextElement(By cssSelector) {
		super(ElementType.INPUT_TEXT, cssSelector);
	}

	public void setInputText(String text) {
		getElement().sendKeys(text);
	}

	public void setInputKeys(Keys key) {
		getElement().sendKeys(key);
	}
	
	public void clearInputText() {
		getElement().clear();
	}

	public void submit() {
		getElement().submit();
	}
	
}
