package com.example.selenium.pages.features.registration;

public enum MaritalStatus {
	SINGLE,
	MARRIED,
	DIVORCED
}
