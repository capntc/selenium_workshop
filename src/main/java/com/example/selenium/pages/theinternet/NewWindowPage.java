package com.example.selenium.pages.theinternet;

import com.example.core.logger.BFLogger;
import com.example.selenium.core.BasePage;

public class NewWindowPage extends BasePage {

    @Override
    public boolean isLoaded() {
	BFLogger.logDebug("is loaded");
	return getDriver().getCurrentUrl().equals("http://the-internet.herokuapp.com/windows/new");
    }

    @Override
    public void load() {
	BFLogger.logDebug("load page");
	getDriver().get("http://the-internet.herokuapp.com/windows/new");
    }

    @Override
    public String pageTitle() {
	return getDriver().getTitle();
    }
}
