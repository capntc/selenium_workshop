package com.example.selenium.pages.theinternet;

import org.openqa.selenium.By;
import com.example.core.logger.BFLogger;
import com.example.selenium.core.BasePage;
import com.example.selenium.core.newDrivers.elementType.DropdownListElement;

public class DropdownPage extends BasePage{

	private static final By selectorDropdownList = By.cssSelector("#dropdown");

	@Override
	public boolean isLoaded() {
		return getDriver().getCurrentUrl().contains("dropdown");
	}

	@Override
	public void load() {
		BFLogger.logDebug("load()");	
	}

	@Override
	public String pageTitle() {
		return "The Internet";
	}
	
	public void setValueOnDropdownList(int valueOfDropdown) {
		DropdownListElement dropdown = getDriver().elementDropdownList(selectorDropdownList);
		dropdown.selectDropdownByIndex(valueOfDropdown);
	}
	
	public String getTextFromDropdownList() {
		DropdownListElement dropdown = getDriver().elementDropdownList(selectorDropdownList);
		return dropdown.getFirstSelectedOptionText();
	}
}
