package com.example.selenium.pages.theinternet;

import com.example.core.logger.BFLogger;
import com.example.selenium.core.BasePage;

public class ElementalSeleniumPage extends BasePage{

	@Override
	public boolean isLoaded() {
		return pageTitle().contains("Elemental Selenium: Receive a Free, Weekly Tip on Using Selenium like a Pro");
	}

	@Override
	public void load() {
		BFLogger.logDebug("load()");
	}

	@Override
	public String pageTitle() {
		return getDriver().getTitle();
	}
}