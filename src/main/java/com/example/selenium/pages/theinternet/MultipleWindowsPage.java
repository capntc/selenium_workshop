package com.example.selenium.pages.theinternet;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.example.core.logger.BFLogger;
import com.example.selenium.core.BasePage;

public class MultipleWindowsPage extends BasePage {

    private final static By selectorLink = By.cssSelector("#content > div > a");

    @Override
    public boolean isLoaded() {
	BFLogger.logDebug("is loaded");
	return getDriver().getCurrentUrl().equals("http://the-internet.herokuapp.com/windows");
    }

    @Override
    public void load() {
	BFLogger.logDebug("load page");
	getDriver().get("http://the-internet.herokuapp.com/windows");
    }

    @Override
    public String pageTitle() {
	return getDriver().getTitle();
    }

    public NewWindowPage clickHereLink() {
	WebElement elementLink = getDriver().findDynamicElement(selectorLink);
	elementLink.click();
	return new NewWindowPage();
    }
}
