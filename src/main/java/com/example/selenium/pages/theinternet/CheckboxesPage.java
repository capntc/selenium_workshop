package com.example.selenium.pages.theinternet;

import org.openqa.selenium.By;

import com.example.core.logger.BFLogger;
import com.example.selenium.core.BasePage;
import com.example.selenium.core.newDrivers.elementType.CheckBox;

public class CheckboxesPage extends BasePage {

    private final static By selectorCheckboxesForm = By.cssSelector("#checkboxes");

    @Override
    public boolean isLoaded() {
	BFLogger.logDebug("The checkboxes page is loaded.");
	return getDriver().getCurrentUrl().equals("http://the-internet.herokuapp.com/checkboxes");
    }

    @Override
    public void load() {
	BFLogger.logDebug("load");
	getDriver().get("http://the-internet.herokuapp.com/checkboxes");

    }

    @Override
    public String pageTitle() {
	return getDriver().getTitle();
    }

    public boolean isElementCheckboxesVisible() {
	getDriver().elementCheckbox(selectorCheckboxesForm).isDisplayed();
	return true;
    }

    public boolean isCheckboxSelectedBefore(int index) {
	CheckBox elementCheckbox = getDriver().elementCheckbox(selectorCheckboxesForm);
	return elementCheckbox.isCheckBoxSetByIndex(index);
    }

    public void thickCheckbox(int index) {
	getDriver().elementCheckbox(selectorCheckboxesForm).setCheckBoxByIndex(index);
    }

    public boolean isCheckboxSelectedAfter(int index) {
	CheckBox elementCheckbox = getDriver().elementCheckbox(selectorCheckboxesForm);
	return elementCheckbox.isCheckBoxSetByIndex(index);
    }

 
    public void unthickCheckbox(int index) {
	getDriver().elementCheckbox(selectorCheckboxesForm).unsetCheckBoxByIndex(index);
    }
  
}
