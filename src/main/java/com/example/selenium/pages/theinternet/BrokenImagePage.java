package com.example.selenium.pages.theinternet;

import org.openqa.selenium.By;

import com.example.core.logger.BFLogger;
import com.example.selenium.core.BasePage;

public class BrokenImagePage extends BasePage{
	
	private static final By[] selectorsImages = {By.cssSelector("div > img:nth-child(2)"),
												 By.cssSelector("div > img:nth-child(3)"),
												 By.cssSelector("div > img:nth-child(4)")};
	
	@Override
	public boolean isLoaded() {
		return getDriver().getCurrentUrl().contains("broken_images");
	}

	@Override
	public void load() {
		BFLogger.logDebug("load()");
	}

	@Override
	public String pageTitle() {
		return "The Internet";
	}
	
	public int getHeightOfImage(int imageIndex) {
		return getDriver().findDynamicElement(selectorsImages[imageIndex]).getSize().getHeight();	
	}
	
	public int getWidthOfImage(int imageIndex) {
		return getDriver().findDynamicElement(selectorsImages[imageIndex]).getSize().getWidth();
	}
	
}
