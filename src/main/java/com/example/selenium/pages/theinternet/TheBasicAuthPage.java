package com.example.selenium.pages.theinternet;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;

import com.example.core.logger.BFLogger;
import com.example.selenium.core.BasePage;

public class TheBasicAuthPage extends BasePage {

	private static final By selectorTextMessage = By.cssSelector("#content > div > p");

	public TheBasicAuthPage() {
		this(false, "", "");
	}
	
	public TheBasicAuthPage(boolean openByUrl, String login, String password) {
		if (openByUrl) {
			this.enterLoginAndPasswordByUrl(login, password);
		}
	}
			
	@Override
	public boolean isLoaded() {
		BFLogger.logDebug("is loaded");
		return getDriver().getCurrentUrl().equals("http://the-internet.herokuapp.com/basic_auth");
	}

	@Override
	public void load() {
		BFLogger.logDebug("load");
	}

	@Override
	public String pageTitle() {
        return getDriver().getTitle();
	}
	
	private void enterLoginAndPasswordByUrl(String login, String password) {
		getDriver().get("http://" + login + ":" + password + "@the-internet.herokuapp.com/basic_auth");	
	}
	
	public void enterLoginAndPassword(String login, String password) throws AWTException, InterruptedException {		
		 Robot rb = new Robot();
		  
		 Thread.sleep(2000);

		 StringSelection username = new StringSelection(login);
		 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(username, null);            
		 rb.keyPress(KeyEvent.VK_CONTROL);
		 rb.keyPress(KeyEvent.VK_V);
		 rb.keyRelease(KeyEvent.VK_V);
		 rb.keyRelease(KeyEvent.VK_CONTROL);

		 rb.keyPress(KeyEvent.VK_TAB);
		 rb.keyRelease(KeyEvent.VK_TAB);
		 Thread.sleep(2000);

		 StringSelection pwd = new StringSelection(password);
		 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(pwd, null);
		 rb.keyPress(KeyEvent.VK_CONTROL);
		 rb.keyPress(KeyEvent.VK_V);
		 rb.keyRelease(KeyEvent.VK_V);
		 rb.keyRelease(KeyEvent.VK_CONTROL);

		 rb.keyPress(KeyEvent.VK_ENTER);
		 rb.keyRelease(KeyEvent.VK_ENTER); 
		 Thread.sleep(2000);
	}

	public String getMessageValue() {
		return getDriver().findDynamicElement(selectorTextMessage).getText();
	}	
}
