package com.example.selenium.tests.jsoup;

import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;

import com.example.core.logger.BFLogger;
import com.example.core.tests.core.BaseTest;
import com.example.selenium.pages.jsoup.EditableGridPage;

public class JsoupTests extends BaseTest {
	
	private static EditableGridPage editableGridPage;
	
	@BeforeClass
	public static void setUpBeforeClass() {
		BFLogger.logDebug("OPEN BROWSER AND ENTER HTTP://WWW.EDITABLEGRID.NET/EN/");
		editableGridPage = new EditableGridPage();
		BFLogger.logDebug("VERIFY PAGE IS DISPLAYED");
		assertTrue("Page not opened", editableGridPage.isLoaded());
	}
	
	@Test
	public void Test1_exercise1_printAllCells() {
		BFLogger.logInfo("[TEST1] - Print all cells");
		editableGridPage.printAllCells();
	}
	
	
	@Override
	public void setUp() {
	}
	
	@Override
	public void tearDown() {
	}
	
}
