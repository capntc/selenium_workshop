Feature: Web Element - Button

  Scenario: Check button's funcionalities

    Given I'm as unlogger user on AUTOMATION PRACTICE FORM site
    And I should see Submit Button
    When I check Submit Button type
    And I click on Submit Button
    Then I should be on Submit site