package com.example.selenium.tests.theinternet;

import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;
import com.example.core.logger.BFLogger;
import com.example.core.tests.core.BaseTest;
import com.example.selenium.core.BasePage;
import com.example.selenium.pages.theinternet.ABtestPage;
import com.example.selenium.pages.theinternet.TheInternetPage;

public class ABtestingTest extends BaseTest {

	private static TheInternetPage theInternetPage;
	private static ABtestPage abTestPage;
	
	@BeforeClass
	public static void setUpBeforeClass() {	
		BFLogger.logDebug("Step1 - open Chrome browser");
		BFLogger.logDebug("Step2 - load http://the-internet.herokuapp.com/ page");
		theInternetPage = new TheInternetPage();
		assertTrue("The-internet page is not loaded",getDriver().getCurrentUrl().contains("the-internet.herokuapp.com"));
	}
	
	@Override
	public void setUp() {}

	@Override
	public void tearDown() {
		BasePage.navigateBack();
	}
	
	@Test
	public void shouldOpenABtestWhenClickABtestingLink() {
		BFLogger.logDebug("Step3 - click ABtesting link");
		theInternetPage.clickABtestingLink();
		getDriver().waitForPageLoaded();
		assertTrue("AB testing page is not loaded", getDriver().getCurrentUrl().contains("abtest"));
	}
	
	@Test
	public void shouldOpenElementalSeleniumPageWhenClickElementalSeleniumLink() {
		BFLogger.logDebug("Step3 - click ABtesting link");
		abTestPage = theInternetPage.clickABtestingLink();
		getDriver().waitForPageLoaded();
		assertTrue("AB testing page is not loaded", getDriver().getCurrentUrl().contains("abtest"));
		BFLogger.logDebug("Step4 - click Elemental Selenium link");
		abTestPage.clickElementalSeleniumLink();
		BFLogger.logDebug("Step3 - switch tab in browser to new opened Elemental Selenium link");
		abTestPage.switchToNextTab();
		assertTrue("Selenium Elemental page is not loaded", getDriver().getTitle().contains("Elemental Selenium"));
	}
}
