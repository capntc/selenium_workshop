package com.example.selenium.tests.theinternet;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.example.core.logger.BFLogger;
import com.example.core.tests.core.BaseTest;
import com.example.selenium.core.BasePage;
import com.example.selenium.pages.theinternet.BrokenImagePage;
import com.example.selenium.pages.theinternet.TheInternetPage;

public class BrokenImagesTest extends BaseTest {
	
	private TheInternetPage theInternetPage;
	private BrokenImagePage brokenImagePage;
	private static final int CorrectHeight = 90;
	private static final int CorrectWidth = 120;
	
	@Override
	public void setUp() {
		BFLogger.logDebug("Step1 - open Chrome browser");
		BFLogger.logDebug("Step2 - open web page http://the-internet.herokuapp.com/");
		theInternetPage = new TheInternetPage();
		assertTrue("The-internet page is not loaded", getDriver().getCurrentUrl()
				.contains("the-internet.herokuapp.com"));
	}
	
	@Override
	public void tearDown() {
		BFLogger.logDebug("Step5 - navigate back to The-Internet page");
		BasePage.navigateBack();
	}
	
	@Test
	public void verifyIfImagesSizesAreCorrect() {
		BFLogger.logDebug("Step3 - click Broken Image link and open Broken Image page");
		brokenImagePage = theInternetPage.clickBrokenImageLink();
		for (int i = 0; i < 3; i++) {
			BFLogger.logDebug("Step4 - check sizes of image with index: " + i);
			assertTrue("Height of image with index " + i + " is incorrect", brokenImagePage.getHeightOfImage(i) == CorrectHeight);
			assertTrue("Width of image with index " + i + " is incorrect", brokenImagePage.getWidthOfImage(i) == CorrectWidth);
		}
	}
	
}
