package com.example.selenium.tests.theinternet;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.example.core.logger.BFLogger;
import com.example.core.tests.core.BaseTest;
import com.example.selenium.pages.theinternet.MultipleWindowsPage;
import com.example.selenium.pages.theinternet.NewWindowPage;
import com.example.selenium.pages.theinternet.TheInternetPage;

public class MultipleWindowsTest extends BaseTest {

    private static TheInternetPage theInternetPage;
    private static MultipleWindowsPage multipleWindowsPage;
    private static NewWindowPage newWindowPage;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
	BFLogger.logDebug("Step 1: Open the Url http://the-internet.herokuapp.com/");
	theInternetPage = new TheInternetPage();
	BFLogger.logDebug("Step 2: Verify if Url http://the-internet.herokuapp.com/ opens");
	assertTrue("The Internet Page was not open", theInternetPage.isLoaded());
	BFLogger.logDebug("Step 3: Click on the Multiple Windows link");
	multipleWindowsPage = theInternetPage.clickmultipleWindowsPageLink();
	BFLogger.logDebug("Step 4: Verify if the Url http://the-internet.herokuapp.com/windows");
	assertTrue("The Multiple Windows Page was not open", multipleWindowsPage.isLoaded());
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }

    @Test
    public void verifyIfNewBrowserWindowOpen() {
	newWindowPage = multipleWindowsPage.clickHereLink();
	assertTrue("New browser window doesn't open", newWindowPage.isLoaded());
    }
}
