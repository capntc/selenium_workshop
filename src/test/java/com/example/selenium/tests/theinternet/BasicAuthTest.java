package com.example.selenium.tests.theinternet;

import static org.junit.Assert.*;

import java.awt.AWTException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.example.core.logger.BFLogger;
import com.example.core.tests.core.BaseTest;
import com.example.selenium.pages.theinternet.TheBasicAuthPage;
import com.example.selenium.pages.theinternet.TheInternetPage;

public class BasicAuthTest extends BaseTest {
	
	private static TheInternetPage theInternetPage;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}
	
	@Override
	public void setUp() {
		BFLogger.logDebug("Open url http://the-internet.herokuapp.com/");
		theInternetPage = new TheInternetPage();
		BFLogger.logDebug("Check if page is loaded");
		assertTrue("The Internet Page isn't loaded", theInternetPage.isLoaded());
	}

	@Override
	public void tearDown() {	
	}
	
	@Test
	public void shouldLogInWhenCredentialsCorrect() throws InterruptedException, AWTException {
		String login = "admin";
		String password = "admin";
		BFLogger.logDebug("Click on Basic Auth");
		TheBasicAuthPage theBasicAuthPage = theInternetPage.clickBasicAuthLink();
		BFLogger.logDebug("Enter login and password");
		theBasicAuthPage.enterLoginAndPassword(login, password);
		assertTrue("You are not on Basic Auth Page", theBasicAuthPage.isLoaded());
		BFLogger.logDebug("Check if user successfully logged in");
		String message = "Congratulations! You must have the proper credentials.";
		assertEquals("User isn't logged in", message, theBasicAuthPage.getMessageValue());
	}
	
	@Test
	public void shouldLogInWhenCredentialsInURL() {
		String login = "admin";
		String password = "admin";
		BFLogger.logDebug("Go to BasicAuthPage with login and password admin");
		TheBasicAuthPage theBasicAuthPage = new TheBasicAuthPage(true, login, password);
		BFLogger.logDebug("Check if user successfully logged in");
		String message = "Congratulations! You must have the proper credentials.";
		assertEquals("User isn't logged in", message, theBasicAuthPage.getMessageValue());
	}
}
