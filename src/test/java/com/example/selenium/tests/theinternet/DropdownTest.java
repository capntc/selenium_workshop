package com.example.selenium.tests.theinternet;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.example.core.logger.BFLogger;
import com.example.core.tests.core.BaseTest;
import com.example.selenium.core.BasePage;
import com.example.selenium.pages.theinternet.DropdownPage;
import com.example.selenium.pages.theinternet.TheInternetPage;

public class DropdownTest extends BaseTest{

	private TheInternetPage theInternetPage;
	private DropdownPage dropdownPage;
	private static final String correctValueOneOnDropdownList = "Option 1";
	private static final String correctValueTwoOnDropdownList = "Option 2";
	
	@Override
	public void setUp() {
		BFLogger.logDebug("Step1 - open Chrome browser");
		BFLogger.logDebug("Step2 - open web page http://the-internet.herokuapp.com/");
		theInternetPage = new TheInternetPage();
		assertTrue("The-internet page is not loaded",getDriver().getCurrentUrl().contains("the-internet.herokuapp.com"));
	}

	@Override
	public void tearDown() {
		BFLogger.logDebug("Step5 - navigate back to The-Internet page");
		BasePage.navigateBack();
	}
	
	@Test
	public void shouldDisplayCorrectTextOnDropdownWhenChooseValueOne() {
		BFLogger.logDebug("Step3 - open 'dropdown' link");
		dropdownPage = theInternetPage.clickDropdownLink();
		dropdownPage.setValueOnDropdownList(1);
		String textOnChoosedValueOfDropdown = dropdownPage.getTextFromDropdownList();
		assertTrue("Choosed value displays incorrect text", textOnChoosedValueOfDropdown.equals(correctValueOneOnDropdownList));
	}
	
	@Test
	public void shouldDisplayCorrectTextOnDropdownWhenChooseValueTwo() {
		BFLogger.logDebug("Step3 - open 'dropdown' link");
		dropdownPage = theInternetPage.clickDropdownLink();
		dropdownPage.setValueOnDropdownList(2);
		String textOnChoosedValueOfDropdown = dropdownPage.getTextFromDropdownList();
		assertTrue("Choosed value displays incorrect text", textOnChoosedValueOfDropdown.equals(correctValueTwoOnDropdownList));
	}
}
