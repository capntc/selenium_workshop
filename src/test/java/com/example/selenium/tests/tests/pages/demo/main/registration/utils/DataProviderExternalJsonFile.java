package com.example.selenium.tests.tests.pages.demo.main.registration.utils;

import com.example.core.tests.TestUtils;
import com.example.core.tests.utils.FormDataContainer;
import com.example.core.utils.datadriven.JsonDriven;

public class DataProviderExternalJsonFile {

	private static final String FILENAME = TestUtils
			.getAbsolutePathFor("AutomatedTestingFramework/src/test/resources/com/example/selenium.tests.levelup.olleni_DataDriven.zadanie12/demoqaLoginData.json");

	public static Object[] provide() {
		return JsonDriven.provide(FILENAME, FormDataContainer[].class);
	}
}
