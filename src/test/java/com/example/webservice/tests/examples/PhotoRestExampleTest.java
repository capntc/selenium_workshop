package com.example.webservice.tests.examples;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;

import org.hamcrest.Matcher;
import org.junit.Test;

import com.example.webservice.tests.models.PhotoServiceObjectModel;

import io.restassured.matcher.ResponseAwareMatcher;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class PhotoRestExampleTest {

	@Test
	public void getPhoto() {

		PhotoServiceObjectModel photoServiceObjectModel = new PhotoServiceObjectModel(
				com.example.webservice.service.WebServiceDriverManager.getRequestSpecyfication());

		// Excecute your method from webservice object model
		Response response = photoServiceObjectModel.getPhoto(12);
		// I can look up response
		System.out.println(response.asString());
		// or directly
		response.prettyPrint();
		// We can get body object
		ResponseBody body = response.getBody();
		// and look up
		System.out.println(body.asString());
		// or like before
		body.prettyPrint();

		assertEquals(12, body.jsonPath().getInt("id"));

		assertEquals("mollitia soluta ut rerum eos aliquam consequatur perspiciatis maiores",
				body.jsonPath().getString("title"));

		// or with rest assured library using response directly and create your
		// own matcher

		response.then().body("url", new ResponseAwareMatcher() {
			@Override
			public Matcher matcher(ResponseBody response) throws Exception {
				return equalTo("http://localhost:8080/" + response.path("userId"));
			}
		});
	}
	
	
}
