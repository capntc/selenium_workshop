package com.example.webservice.tests.models;

import org.json.JSONObject;

import com.example.selenium.pages.enums.PageSubURLsEnum;
import com.example.webservice.service.RestServiceObjectModel;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class PhotoServiceObjectModel extends RestServiceObjectModel {

	public static final String servicePath = "photos";

	public PhotoServiceObjectModel(RequestSpecification requestSpecification) {
		super(requestSpecification, PageSubURLsEnum.WEB_SERVICE.subURL(), servicePath);
		
		System.out.println(getServiceUrl());
	}

	public Response getPhoto(int photoNumber) {
		return getRequestSpecyfication().when().get(getServiceUrl() + "/" + photoNumber);
	}

	public Response sendPhoto(JSONObject photo) {
		return getRequestSpecyfication().body(photo.toString()).when().post(getServiceUrl());
	}

	public Response updatePhoto(JSONObject photo) {
		return getRequestSpecyfication().body(photo).when().put(getServiceUrl() + "/" + photo.getInt("id"));
	}

	public Response deletePhoto(JSONObject photo) {
		return getRequestSpecyfication().when().delete(getServiceUrl() + "/" + photo.getInt("id"));
	}

	public Response getUsersPosts(int userId) {
		return getRequestSpecyfication().when().get(getServiceUrl() + "?userId=" + userId);
	}

}
