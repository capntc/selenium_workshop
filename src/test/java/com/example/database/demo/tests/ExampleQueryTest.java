package com.example.database.demo.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.example.core.tests.core.BaseTest;
import com.example.database.demo.OracleDatabaseDemo;



public class ExampleQueryTest extends BaseTest {

	OracleDatabaseDemo oracleDb;

	@Override
	public void setUp() {

	}
	
	@Test
	public void testShouldExecuteQueryToGetAllEmployeesFromOracleDB() {

		// given
		oracleDb = new OracleDatabaseDemo();
		int expectedCount = 107;
		String expectedFirstEmployeeFirstName = "Donald";
		String expectedFirstEmployeeLastName = "OConnell";
		int expectedFirstEmployeeId = 198;
		
		// when
		oracleDb.getEmplTable().getAllEmployeesQuery().execute();
		int count = oracleDb.getEmplTable().getAllEmployeesQuery().getEmployeesIds().size();
		int id = oracleDb.getEmplTable().getAllEmployeesQuery().getEmployeesIds().get(0);
		String firstName = oracleDb.getEmplTable().getAllEmployeesQuery().getEmployeesFirstNames().get(0);
		String lastName = oracleDb.getEmplTable().getAllEmployeesQuery().getEmployeesLastNames().get(0);

		// then
		assertEquals(expectedCount, count);
		assertEquals(expectedFirstEmployeeFirstName, firstName);
		assertEquals(expectedFirstEmployeeLastName, lastName);
		assertEquals(expectedFirstEmployeeId, id);
	}

	@Override
	public void tearDown() {

	}

}
