package com.example.security;

import static io.restassured.RestAssured.given;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.google.common.base.Preconditions;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.parsing.Parser;

public class SessionManager {

	// TODO: refactor when we have real injection
	private static final SessionManager sessionManager = new SessionManager();
	private Map<SessionEnum, Headers> authData = new HashMap<>();

	private SessionManager() {
		for (SessionEnum session : SessionEnum.values()) {
			if (session.isAuthenticationRequired()) {
				authenticate(session);
			}
		}
	}
	
	private void authenticate(SessionEnum session) {
		RestAssured.defaultParser = Parser.TEXT;
		
		JSONObject request = new JSONObject();
		request.put("username", session.getUser());
		request.put("password", session.getPassword());
		
		Headers headers = 
			given(new RequestSpecBuilder()
				     .setBody(request.toString())
				     .setBaseUri(Configuration.SERVER_ORIGIN)
				     .setBasePath("mythaistar/login")
				     .build()).log().all().
			when()
				.post().getHeaders();
		
		if (! headers.hasHeaderWithName("Authorization")) {
			throw new RuntimeException("No authorization header found");
		}
		
		Header authHeader = headers.get("Authorization");
		Headers authHeaders = new Headers(authHeader);
		authData.put(session, authHeaders);
	}

	public static SessionManager getInstance() {
		return sessionManager;
	}
	
	public RequestSpecBuilder initBuilder(SessionEnum session) {
		Preconditions.checkNotNull(session);
		if (!authData.containsKey(session)) {
			return initBuilder();
		}
		
		RequestSpecBuilder result =  new RequestSpecBuilder();
		for (Header header : authData.get(session).asList()) {
			result.addHeader(header.getName(), header.getValue());
		}
		return result;
	}
	
	public RequestSpecBuilder initBuilder() {
		return new RequestSpecBuilder();
	}
	
	public Headers getAuthHeaders(SessionEnum session) {
		return authData.get(session);
	}
}
