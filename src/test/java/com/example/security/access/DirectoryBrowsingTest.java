package com.example.security.access;

import static io.restassured.RestAssured.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.example.security.Configuration;
import com.example.security.SessionEnum;
import com.example.security.SessionManager;

/**
 * The test verifies that directory browsing is disabled.
 * 
 * OWASP ASVS requirement V4.5: Verify that directory browsing is disabled 
 * unless deliberately desired. Additionally, applications should not allow 
 * discovery or disclosure of file or directory metadata, such as Thumbs.db, 
 * .DS_Store, .git or .svn folders.
 * 
 * Purpose: You don't want the attacker to learn about the system more then
 * needed and he certainly does not need to list files in the directories
 * on your server (unless you deliberately desire this feature). Putting 
 * private content of your e.g. .git folder into your distribution package 
 * to be uploaded to the server and downloaded by the attacker because of 
 * directory browsing feature is a mortal sin on its own.
 *
 * @author Marek Puchalski, Capgemini
 */
@RunWith(Parameterized.class)
public class DirectoryBrowsingTest {

	// TODO: this needs to be replaced with dependency injection
	private SessionManager sessionManager = SessionManager.getInstance();

	private SessionEnum session;
	private String path;
	private String origin;
	private int statusCode;

	@Parameters(name = "{index}: {0}, {1}, {2}, {3}")
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] { 
			{ SessionEnum.ANON, Configuration.CLIENT_ORIGIN, "/assets/images/", 403 }, 
			{ SessionEnum.ANON, Configuration.SERVER_ORIGIN, "/mythaistar/services/rest", 403 },
		});
	}

	public DirectoryBrowsingTest(SessionEnum session, String origin, String path, int statusCode) {
		this.session = session;
		this.origin = origin;
		this.path = path;
		this.statusCode = statusCode;
	}
	
	@Test
	public void testHeader() {
		given(sessionManager
				.initBuilder(session)
				.setBaseUri(origin)
				.setBasePath(path)
				.build())
		.when()
			.get()
		.then()
			.statusCode(statusCode);
	}
}
