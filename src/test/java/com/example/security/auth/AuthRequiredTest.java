package com.example.security.auth;

import static io.restassured.RestAssured.given;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.example.security.Configuration;
import com.example.security.SessionEnum;
import com.example.security.SessionManager;

import io.restassured.http.Method;

/**
 * The test verifies, that protected resources can not be accessed by unauthenticated
 * users.
 * 
 * OWASP ASVS requirement V2.1: Verify all pages and resources by default
 * require authentication except those specifically intended to be public
 * (Principle of complete mediation)
 * 
 * Purpose: This is one of the most basic protection mechanisms. Unit test
 * verifies only a part of this requirement (resources that require authentication -
 * enforce authentication). The second part of this requirement ("by default
 * require") must be validated manually.
 * 
 * Read also: [1]
 * https://www.owasp.org/index.php/Authentication_Cheat_Sheet
 *
 * @author Marek Puchalski, Capgemini
 */
@RunWith(Parameterized.class)
public class AuthRequiredTest {

	// TODO: this needs to be replaced with dependency injection
	private SessionManager sessionManager = SessionManager.getInstance();

	private SessionEnum session;
	private String path;
	private String origin;
	private Method method;
	private String contentType;
	private String body;
	private int statusCode;

	@Parameters(name = "{index}: {0}, {1}, {2}, {3}")
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] { 
			
			//Negative case
			{  SessionEnum.ANON
		     , Configuration.SERVER_ORIGIN
		     , "/mythaistar/services/rest/ordermanagement/v1/order/search"
		     , Method.POST
		     , "application/json"
		     , "{\"pagination\":{\"size\":8,\"page\":1,\"total\":1},\"sort\":[]}" // Body
		     , 403 // Code
		    },
			
			//Positive case
			{  SessionEnum.WAITER
			 , Configuration.SERVER_ORIGIN
			 , "/mythaistar/services/rest/ordermanagement/v1/order/search"
			 , Method.POST
			 , "application/json"
			 , "{\"pagination\":{\"size\":8,\"page\":1,\"total\":1},\"sort\":[]}" // Body
			 , 200 // Code
			}
		});
	}

	public AuthRequiredTest(SessionEnum session, String origin, String path, 
			Method method, String contentType, String body, int statusCode) {
		this.session = session;
		this.origin = origin;
		this.path = path;
		this.method = method;
		this.contentType = contentType;
		this.body = body;
		this.statusCode = statusCode;
	}
	
	@Test
	public void testHeader() {
		given(sessionManager
				.initBuilder(session)
				.setBaseUri(origin)
				.setBasePath(path)
				.addHeader("Content-Type", contentType)
				.setBody(body)
				.build())
		.when()
			.request(method)
		.then()
			.statusCode(statusCode);
	}
}
