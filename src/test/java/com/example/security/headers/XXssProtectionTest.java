package com.example.security.headers;

import static io.restassured.RestAssured.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.example.security.Configuration;
import com.example.security.SessionEnum;
import com.example.security.SessionManager;

/**
 * The test verifies the presence and proper configuration of the
 * X-XSS-Protection header.
 * 
 * OWASP ASVS requirement V11.8: Verify that the X-XSS-Protection: 1; mode=block
 * header is in place.
 * 
 * Purpose: The header in this form may prevent some cases of reflected XSS. It
 * is supported by the IE [1] and Chrome-based browsers. It is turned on by
 * default, but the header like "1; mode=block" enforces the more rigorous mode
 * where the rendering of the whole page is stopped. This is the preferred mode,
 * as the less rigorous one can introduce vulnerabilities that would not exist
 * otherwise [2][3].
 * 
 * Read also: [1]
 * https://blogs.msdn.microsoft.com/ieinternals/2011/01/31/controlling-the-xss-filter/
 * [2] https://www.slideshare.net/masatokinugawa/xxn-en [3]
 * https://www.quora.com/How-effective-is-x-xss-protection-response-header
 *
 * @author Marek Puchalski, Capgemini
 */
@RunWith(Parameterized.class)
public class XXssProtectionTest {

	// TODO: this needs to be replaced with dependency injection
	private SessionManager sessionManager = SessionManager.getInstance();

	private SessionEnum session;
	private String path;
	private String origin;

	@Parameters(name = "{index}: {0}, {1}, {2}")
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] { 
			{ SessionEnum.ANON, Configuration.CLIENT_ORIGIN, "/" }, 
			{ SessionEnum.WAITER, Configuration.SERVER_ORIGIN, "/mythaistar/services/rest/security/v1/currentuser/" },
		});
	}

	public XXssProtectionTest(SessionEnum session, String origin, String path) {
		this.session = session;
		this.origin = origin;
		this.path = path;
	}
	
	@Test
	public void testHeader() {
		given(sessionManager
				.initBuilder(session)
				.setBaseUri(origin)
				.setBasePath(path)
				.build())
		.when()
			.get()
		.then()
			.statusCode(200)
			.header("X-XSS-Protection", "1; mode=block");
	}
}
