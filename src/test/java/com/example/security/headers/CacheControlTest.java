package com.example.security.headers;

import static io.restassured.RestAssured.given;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.example.security.Configuration;
import com.example.security.SessionEnum;
import com.example.security.SessionManager;

import io.restassured.http.Method;

/**
 * The test verifies, that the cache-control header is configured properly.
 * 
 * OWASP ASVS requirement V9.4: Verify that the application sets appropriate anti-caching 
 * headers as per the risk of the application, such as the following:
 * Expires: Tue, 03 Jul 2001 06:00:00 GMT
 * Last-Modified: {now} GMT
 * Cache-Control: no-store, no-cache, must-revalidate, max-age=0
 * Cache-Control: post-check=0, pre-check=0
 * Pragma: no-cache
 * 
 * Purpose: Sensitive information must not be cached or stored locally in the browser. 
 * Anyone with physical access to the machine could potentially extract the data even after 
 * the session of a legitimate user is closed.
 *
 * @author Piotr Stankiewicz, Capgemini
 */
@RunWith(Parameterized.class)
public class CacheControlTest {

	// TODO: this needs to be replaced with dependency injection
	private SessionManager sessionManager = SessionManager.getInstance();

	private SessionEnum session;
	private String path;
	private String origin;
	private Method method;
	private String contentType;
	private String body;
	private int statusCode;
	
	@Parameters(name = "{index}: {0}, {1}, {2}, {3}")
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] { 
			{  SessionEnum.WAITER
		     , Configuration.SERVER_ORIGIN
		     , "/mythaistar/services/rest/ordermanagement/v1/order/search"
		     , Method.POST
		     , "application/json"
		     , "{\"pagination\":{\"size\":8,\"page\":1,\"total\":1},\"sort\":[]}" // Body
		     , 200 // Code
		    }, 
			{  SessionEnum.ANON
			 , Configuration.SERVER_ORIGIN
			 , "/mythaistar/services/rest/dishmanagement/v1/dish/search"
			 , Method.POST
			 , "application/json"
			 , "{\"sort\":[],\"categories\":[]}" // Body
			 , 200 // Code
			},
		});
	}

	public CacheControlTest(SessionEnum session, String origin, String path, 
			Method method, String contentType, String body, int statusCode) {
		this.session = session;
		this.origin = origin;
		this.path = path;
		this.method = method;
		this.contentType = contentType;
		this.body = body;
		this.statusCode = statusCode;
	}
	
	@Test
	public void testHeader() {
		given(sessionManager
				.initBuilder(session)
				.setBaseUri(origin)
				.setBasePath(path)
				.setBody(body)
				.addHeader("content-type", contentType)
				.build())
		.when()
			.request(method)
		.then()
			.statusCode(statusCode)
			.header("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate")
			.header("Pragma", "no-cache")
			.header("Expires", "0");
	}
}
