package com.example.security;

public enum SessionEnum {
	ANON, WAITER(Configuration.USER_WAITER, Configuration.PASSWD_WAITER);
	
	private String user;
	private String password;
	
	private SessionEnum() {
		// nothing
	}
	
	private SessionEnum(String user, String password) {
		this.user = user;
		this.password = password;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isAuthenticationRequired() {
		return user != null && password != null;
	}
	
}
